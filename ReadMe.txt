UPS - Unit testing, school project

- Testing classes and methods that are invoked as result of an
  interaction with the form
- Testing data entry in .xml file
- Using Mock and Stub objects for currency conversion