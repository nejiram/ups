﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadatak3;
using System.Collections.Generic;

namespace TestProjekat
{
    [TestClass]
    public class UnitTest1
    {
        Izracunaj izracunaj = new Izracunaj();
        PosiljkeKolekcija kolekcija = new PosiljkeKolekcija();

        [TestMethod]
        public void KreiranjePosiljkeTest()
        {
            Posiljka pos = new Posiljka("bih", "hr", "333", "20", "20", 5, "44");
            kolekcija.dodajPosiljku(pos);
        }

        [TestMethod]
        public void BrisanjePosiljkeTest()
        {
            Posiljka praznaP = new Posiljka();
            Posiljka posiljka = new Posiljka("bih", "hr", "443", "15", "30", 2, "4");
            Posiljka pos = new Posiljka("bih", "hr", "333", "20", "20", 5, "44");
            kolekcija.dodajPosiljku(posiljka);
            kolekcija.dodajPosiljku(pos);
            kolekcija.obrisiPosiljku(pos);
            List<Posiljka> lista = kolekcija.Posiljke;
            if (lista.Contains(pos)) throw new ArgumentException("ne radi brisanje");
        }

        [TestMethod]
        public void BrisanjePoIndexuPosiljkeTest()
        { 
            Posiljka posiljka = new Posiljka("bih", "hr", "443", "15", "30", 2, "4");
            Posiljka pos = new Posiljka("bih", "hr", "333", "20", "20", 5, "44");
            kolekcija.dodajPosiljku(posiljka);
            kolekcija.dodajPosiljku(pos);
            kolekcija.obrisiPoIndexu(0);
            List<Posiljka> lista = kolekcija.Posiljke;
            
            if (lista.Contains(posiljka)) throw new ArgumentException("ne radi brisanje");
        }

        [TestMethod]
        public void ImaLiPosiljkeTest()
        {
            if (kolekcija.imaLiPosiljki()) throw new ArgumentException("ne radi imaLiPosiljki");
        }


        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeIzuzetakOdakleTest()
        {
            Posiljka pos = new Posiljka("b3333ih", "hr", "333", "20", "20", 5, "44");
            //sve moraju biti slova 
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeIzuzetakDokleTest()
        {
            Posiljka pos = new Posiljka("BIH", "BIH66", "333", "20", "20", 5, "44");
            //sve moraju biti slova
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeIzuzetakDisTest1()
        {
            Posiljka pos = new Posiljka("BIH", "HR", "3eee33", "20", "20", 5, "44");
            //sve moraju biti brojev
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeIzuzetakDisTest2()
        {
            Posiljka pos = new Posiljka("BIH", "BIH", "-33", "20", "20", 5, "44");
            //sve moraju biti brojevi veci od 0
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeIzuzetakTezinaTest1()
        {
            Posiljka pos = new Posiljka("bih", "hr", "333", "2rrrrr0", "20", 5, "44");
            //sve moraju biti brojevi
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeIzuzetakTest2()
        {
            Posiljka pos = new Posiljka("bih", "hr", "33", "-20", "20", 5, "44");
            //sve moraju biti brojevi veci od 0
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeIzuzetakTaksaTest1()
        {
            Posiljka pos = new Posiljka("bih", "hr", "333", "20", "2lksdf0", 5, "44");
            //sve moraju biti brojevi
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeTaksaTest2()
        {
            Posiljka pos = new Posiljka("bih", "hr", "33", "20", "-20", 5, "44");
            //sve moraju biti brojevi veci od 0
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeCijenaTest1()
        {
            Posiljka pos = new Posiljka("bih", "hr", "33", "20", "20", 5, "ggg44");
            //sve moraju biti brojevi veci od 0
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void KreiranjePosiljkeCijenaTest2()
        {
            Posiljka pos = new Posiljka("bih", "hr", "33", "20", "20", 5, "-44");
            //sve moraju biti brojevi veci od 0
        }

        //drugi konstruktor posiljke
        [TestMethod]
        public void KreiranjePosiljkeTest2()
        {
            Posiljka pos = new Posiljka("BIH", "BIH", "34", "43", "14", 3, "33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void PosiljkaOdakleIzuzetakTest2()
        {
            Posiljka pos = new Posiljka("33BIH", "BIH", "34", "43", "14", 3, "33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void PosiljkaDokleIzuzetakTest2()
        {
            Posiljka pos = new Posiljka("BIH", "B234IH", "34", "43", "14", 3, "33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void PosiljkaDistancaIzuzetakTest2()
        {
            Posiljka pos = new Posiljka("BIH", "B234IH", "f34", "43", "14", 3, "33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void PosiljkaDistancaIzuzetakTest3()
        {
            Posiljka pos = new Posiljka("BIH", "BIH", "-34", "43", "14", 3, "33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void PosiljkaTezinaIzuzetakTest2()
        {
            Posiljka pos = new Posiljka("BIH", "BIH", "34", "lko43", "14", 3, "33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void PosiljkaTezinaIzuzetakTest3()
        {
            Posiljka pos = new Posiljka("BIH", "BIH", "34", "-43", "14", 3, "33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void PosiljkaCijenaIzuzetakTest2()
        {
            Posiljka pos = new Posiljka("BIH", "BIH", "34", "43", "14", 3, "ddd33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void PosiljkaCijenaIzuzetakTest3()
        {
            Posiljka pos = new Posiljka("BIH", "BIH", "34", "43", "14", 3, "-33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
        }

        [TestMethod]
         public void PosiljkaGetTest2()
        {
            Posiljka pos = new Posiljka("BIH", "BIH", "34", "43", "14", 3, "33", "han", "mek", "0204998175024", "+38798-098-098", "kom");
            kolekcija.dodajPosiljku(pos);
            String odakle = pos.Odakle;
            String dokle = pos.Dokle;
            double dis = pos.Distanca;
            double tezina = pos.Tezina;
            double taksa = pos.Takse;
            Double hitnost = pos.Hitnost;
            double cijenabez = pos.CijenaBezTakse;
            double cijenasa = pos.CijenaSaTaksom;
            String ime = pos.Ime;
            String prez = pos.Prezime;
            String jmbg = pos.JMBG;
            String tel = pos.Telefon;
            String kom = pos.Komentar;
            PosiljkeKolekcija nova = new PosiljkeKolekcija();
            Posiljka novi = new Posiljka(odakle, dokle, dis.ToString(), tezina.ToString(), taksa.ToString(), hitnost, cijenasa.ToString(), ime, prez, jmbg, tel, kom);
            nova.dodajPosiljku(novi);
        //    Assert.AreSame(nova, kolekcija);
        }

        [TestMethod]
        public void PosiljkaSetTest2()
        {
            Posiljka pos = new Posiljka();

            pos.Odakle="BIH";
            pos.Dokle="BIH";
            pos.Distanca=34;
            pos.Tezina=34;
            pos.Takse=6;
            pos.Hitnost=3;
            pos.CijenaBezTakse=44;
            pos.CijenaSaTaksom=44;
            pos.Ime ="ime";
            pos.Prezime="pres";
            pos.JMBG="0204998175024";
            pos.Telefon ="+38767-098-090";
            pos.Komentar="ddd";
            
            kolekcija.dodajPosiljku(pos);
        }


        [TestMethod]
        public void IzracunajCijenuBezTakseTest2()
        {
            Double distanca = 200;
            Double tezina = 40;
            Double hitnost = 3;
            Double rez = izracunaj.izracunajCijenuBezTakse(distanca, tezina, hitnost);
            Double expected = 5.72;
            Assert.AreEqual(expected, rez);
        }

        [TestMethod]
        public void IzracunajCijenuSaTaksomTest1()
        {
            Double distanca = 200;
            Double tezina = 40;
            Double hitnost = 3;
            Double taksa = 10;
            Double rez = izracunaj.izracunajCijenuSaTaksom(distanca, tezina, hitnost,taksa);
            Double expected = 62.92;
            Assert.AreEqual(expected, rez);
        }

        [TestMethod]
        public void IzracunajDistancuTest1()
        {
            Double tezina = 110;
            Double hitnost = 3;
            Double taksa = 10;
            Double cijena = 30;
            Double rez = izracunaj.izracunajDistancu(tezina, hitnost, taksa,cijena);
            Double expected = 34.68;
            Assert.AreEqual(expected, rez);
        }

        [TestMethod]
        public void IzracunajTezinuTest1()
        {
            Double distanca = 100;
            Double hitnost = 3;
            Double taksa = 10;
            Double cijena = 30;
            Double rez = izracunaj.izracunajTezinu(distanca, hitnost, taksa, cijena);
            Double expected =38.13;
            Assert.AreEqual(expected, rez);
        }

        [TestMethod]
        public void IzracunajTaksuTest1()
        {
            Double distanca = 100;
            Double hitnost = 3;
            Double tezina = 40;
            Double cijena = 30;
            Double rez = izracunaj.izracunajTakse(distanca, tezina, hitnost,cijena);
            Double expected = 9.49;
            Assert.AreEqual(expected, rez);
        }

        //osoba validacija
        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void OsobaImeIzuzetakTest1()
        {
            Osoba osoba = new Osoba("22hana", "mekic", "0204998987345", "+38798987-098", "");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void OsobaPrezimeIzuzetakTest1()
        {
            Osoba osoba = new Osoba("hana", "m777ekic", "0204998987345", "+38798987-098", "");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void OsobaJMBGIzuzetakTest1()
        {
            Osoba osoba = new Osoba("hana", "mekic", "020499hjkhs8987345", "+38798987-098", "");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void OsobaJMBGIzuzetakTest2()
        {
            Osoba osoba = new Osoba("hana", "mekic", "02049998987345", "+38798-987-098", "");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void OsobaTelIzuzetakTest1()
        {
            Osoba prazna = new Osoba();
            Osoba osoba = new Osoba("hana", "mekic", "0204998175024", "+387989lka87-098", "");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void OsobaTelIzuzetakTest2()
        {
            Osoba osobaTelefon = new Osoba("ema", "mek", "0204998175024", "0-38498987-228", "");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void OsobaTelIzuzetakTest3()
        {
            Osoba osobaTelefon = new Osoba("ema", "mek", "0204998175024", "+48498-987-228", "");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void OsobaTelIzuzetakTest4()
        {
            Osoba osobaTelefon = new Osoba("ema", "mek", "0204998175024", "+38798-9870228", "");
        }

        [TestMethod]
        public void OsobaTest()
        {
            Osoba osoba = new Osoba("ema", "mek", "0204998175024", "+38798-987-228", "komentar");

        }

        [TestMethod]
        public void OsobaSetTest()
        {
            Osoba osoba = new Osoba();
            osoba.Ime="mujo";
            osoba.Prezime="muj";
            osoba.JMBG="0303303030";
            osoba.Telefon ="+34565444333";
            osoba.Komentar="set";
            if (osoba.Prezime != "muj") throw new ArgumentException("ne valja seter");
        }

        [TestMethod]
        public void OsobaGetTest()
        {
            Osoba osoba = new Osoba("has","haso","0204998175024","+38797-879-987","");

            string ime=osoba.Ime;
            string prez=osoba.Prezime;
            string jmbg=osoba.JMBG;
            string tel=osoba.Telefon;
            string kom=osoba.Komentar;

            if (osoba.Prezime != "haso") throw new ArgumentException("ne valja geter");
        }

    }
}
