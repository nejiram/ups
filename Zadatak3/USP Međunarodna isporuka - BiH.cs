﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zadatak3
{
    public partial class USP_Međunarodna_isporuka___BiH : Form
    {
        PosiljkeKolekcija listaPosiljke;

        public USP_Međunarodna_isporuka___BiH()
        {
            InitializeComponent();
            listaPosiljke = new PosiljkeKolekcija();
        }
        
        private void USP_Međunarodna_isporuka___BiH_Load(object sender, EventArgs e)
        {
            try
            {
                listaPosiljke.Deserijalizacija();
                osvjeziTabelu();
            }
            catch (Exception d)
            {
                MessageBox.Show("Neuspjela deserijalizacija: " + d.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cijenaIsporukeButton_Click(object sender, EventArgs e)
        {
            try
            {
                String odakle = odakleComboBox.Text.ToString();
                String dokle = dokleComboBox.Text.ToString();
                Posiljka p;
                Izracunaj i = new Izracunaj();

                //uneseno sve osim distance
                if (distancaTextBox.Text.Equals("") && !tezinaTextBox.Text.Equals("") && !takseTextBox.Text.Equals("") && !cijenaTextBox.Text.Equals(""))
                {
                    Double distanca = i.izracunajDistancu(Convert.ToDouble(tezinaTextBox.Text), Convert.ToDouble(hitnostTrackBar.Value), Convert.ToDouble(takseTextBox.Text), Convert.ToDouble(cijenaTextBox.Text));

                    distancaTextBox.Text = distanca.ToString();

                    p = new Posiljka(odakle, dokle, distancaTextBox.Text, tezinaTextBox.Text, takseTextBox.Text, hitnostTrackBar.Value, cijenaTextBox.Text);

                }
                // uneseno sve osim tezine
                else if (!distancaTextBox.Text.Equals("") && tezinaTextBox.Text.Equals("") && !takseTextBox.Text.Equals("") && !cijenaTextBox.Text.Equals(""))
                {
                    double tezina = i.izracunajTezinu(Convert.ToDouble(distancaTextBox.Text), Convert.ToDouble(hitnostTrackBar.Value), Convert.ToDouble(takseTextBox.Text), Convert.ToDouble(cijenaTextBox.Text));

                    tezinaTextBox.Text = tezina.ToString();

                    p = new Posiljka(odakle, dokle, distancaTextBox.Text, tezinaTextBox.Text, takseTextBox.Text, hitnostTrackBar.Value, cijenaTextBox.Text);

                }
                //uneseno sve osim takse
                else if (!distancaTextBox.Text.Equals("") && !tezinaTextBox.Text.Equals("") && takseTextBox.Text.Equals("") && !cijenaTextBox.Text.Equals(""))
                {
                    double takse = i.izracunajTakse(Convert.ToDouble(distancaTextBox.Text), Convert.ToDouble(tezinaTextBox.Text), Convert.ToDouble(hitnostTrackBar.Value), Convert.ToDouble(cijenaTextBox.Text));

                    tezinaTextBox.Text = takse.ToString();

                    p = new Posiljka(odakle, dokle, distancaTextBox.Text, tezinaTextBox.Text, takseTextBox.Text, hitnostTrackBar.Value, cijenaTextBox.Text);

                }
                //uneseno sve osim cijene
                else
                {
                    Double cijena = i.izracunajCijenuSaTaksom(Convert.ToDouble(distancaTextBox.Text), Convert.ToDouble(tezinaTextBox.Text), Convert.ToDouble(hitnostTrackBar.Value), Convert.ToDouble(takseTextBox.Text));

                    cijenaTextBox.Text = cijena.ToString();

                    p = new Posiljka(odakle, dokle, distancaTextBox.Text, tezinaTextBox.Text, takseTextBox.Text, hitnostTrackBar.Value, cijenaTextBox.Text);
                }

            }
            catch (Exception d)
            {
                MessageBox.Show("Pogrešan unos: " + d.Message);
            }

            //osvjeziTabelu();
            

        }

        private void registrujPosiljkuButton_Click(object sender, EventArgs e)
        {
            try
            {
                #region GROUPBOX Podaci o posiljci
                String odakle = odakleComboBox.Text.ToString();
                String dokle = dokleComboBox.Text.ToString();
                Posiljka p;
                Izracunaj i = new Izracunaj();

                
                //uneseno sve osim distance
                if (distancaTextBox.Text.Equals("") && !tezinaTextBox.Text.Equals("") && !takseTextBox.Text.Equals("") && !cijenaTextBox.Text.Equals(""))
                {
                    Double distanca = i.izracunajDistancu(Convert.ToDouble(tezinaTextBox.Text), Convert.ToDouble(hitnostTrackBar.Value), Convert.ToDouble(takseTextBox.Text), Convert.ToDouble(cijenaTextBox.Text));

                    distancaTextBox.Text = distanca.ToString();

                    p = new Posiljka(odakle, dokle, distancaTextBox.Text, tezinaTextBox.Text, takseTextBox.Text, hitnostTrackBar.Value, cijenaTextBox.Text);

                }
                // uneseno sve osim tezine
                else if (!distancaTextBox.Text.Equals("") && tezinaTextBox.Text.Equals("") && !takseTextBox.Text.Equals("") && !cijenaTextBox.Text.Equals(""))
                {
                    double tezina = i.izracunajTezinu(Convert.ToDouble(distancaTextBox.Text), Convert.ToDouble(hitnostTrackBar.Value), Convert.ToDouble(takseTextBox.Text), Convert.ToDouble(cijenaTextBox.Text));

                    tezinaTextBox.Text = tezina.ToString();

                    p = new Posiljka(odakle, dokle, distancaTextBox.Text, tezinaTextBox.Text, takseTextBox.Text, hitnostTrackBar.Value, cijenaTextBox.Text);

                }
                //uneseno sve osim takse
                else if (!distancaTextBox.Text.Equals("") && !tezinaTextBox.Text.Equals("") && takseTextBox.Text.Equals("") && !cijenaTextBox.Text.Equals(""))
                {
                    double takse = i.izracunajTakse(Convert.ToDouble(distancaTextBox.Text), Convert.ToDouble(tezinaTextBox.Text), Convert.ToDouble(hitnostTrackBar.Value), Convert.ToDouble(cijenaTextBox.Text));

                    tezinaTextBox.Text = takse.ToString();

                    p = new Posiljka(odakle, dokle, distancaTextBox.Text, tezinaTextBox.Text, takseTextBox.Text, hitnostTrackBar.Value, cijenaTextBox.Text);

                }
                //uneseno sve osim cijene
                else
                {
                    Double cijena = i.izracunajCijenuSaTaksom(Convert.ToDouble(distancaTextBox.Text), Convert.ToDouble(tezinaTextBox.Text), Convert.ToDouble(hitnostTrackBar.Value), Convert.ToDouble(takseTextBox.Text));

                    p = new Posiljka(odakle, dokle, distancaTextBox.Text, tezinaTextBox.Text, takseTextBox.Text, hitnostTrackBar.Value, cijenaTextBox.Text);
                }

                #endregion

                #region GROUPBOX Licni podaci

                String ime = imeTextBox.Text;
                String prezime = prezimeTextBox.Text;
                String jmbg = jmbgTextBox.Text;
                String tel = telefonTextBox.Text;
                String kom = komentarRichTextBox.Text;
                

                #endregion

                Osoba o = new Osoba(ime, prezime, jmbg, tel, kom);
                listaPosiljke.dodajPosiljku(new Posiljka(odakle, dokle, distancaTextBox.Text, tezinaTextBox.Text, takseTextBox.Text, hitnostTrackBar.Value, cijenaTextBox.Text,
                    o.Ime, o.Prezime, o.JMBG, o.Telefon, o.Komentar));
                listaPosiljke.Serijalizacija();

                imeTextBox.Text = "";
                prezimeTextBox.Text = "";
                jmbgTextBox.Text = "";
                telefonTextBox.Text = "";
                komentarRichTextBox.Text = "";
                distancaTextBox.Text = "";
                tezinaTextBox.Text = "";
                takseTextBox.Text = "";
                cijenaTextBox.Text = "";

                osvjeziTabelu();

            }
            catch (Exception d)
            {
                MessageBox.Show("Pogrešan unos: " + d.Message);
            }

            
        }

        private void osvjeziTabelu()
        {
            tabelaDataGrid.DataSource = null;
            tabelaDataGrid.DataSource = listaPosiljke.Posiljke;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void izbrisiButton_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow item in this.tabelaDataGrid.SelectedRows)
                    listaPosiljke.obrisiPoIndexu(item.Index);

                listaPosiljke.Serijalizacija();
                osvjeziTabelu();
            }

            catch (Exception d)
            {
                MessageBox.Show("Neuspjelo brisanje : \n" + d.Message);
            }
        }
    }
}
