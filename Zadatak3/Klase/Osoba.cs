﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace Zadatak3
{
    public class Osoba
    {
        String ime, prezime, jmbg, telefon, komentar;

        #region KONSTRUKTORI

        public Osoba() { }

        public Osoba(String i, String p, String j, String tel, String kom)
        {
            try
            {
                if (!validacijaIme(i))
                    throw new ArgumentException("Ime može sadržavati samo slova!");

                this.ime = i;

                if (!validacijaPrezime(p))
                    throw new ArgumentException("Prezime može sadržavati samo slova!");

                this.prezime = p;

                if (!ValidacijaJMBG(j))
                    throw new ArgumentException("Pogrešan matični broj !");

                this.jmbg = j;

                if (!ValidacijaTel(tel))
                    throw new ArgumentException("Pogrešan format telefona !");

                this.telefon = tel;

                this.komentar = kom;
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region GETERI I SETERI

        public String Ime
        {
            get { return ime; }
            set { ime = value; }
        }

        public String Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }

        public String JMBG
        {
            get { return jmbg; }
            set { jmbg = value; }
        }

        public String Telefon
        {
            get { return telefon; }
            set { telefon = value; }
        }

        public String Komentar
        {
            get { return komentar; }
            set { komentar = value; }
        }

        
        #endregion

        #region VALIDACIJE

        public static Boolean ValidacijaJMBG(String JMBG)
        {
            if (!JMBG.All(char.IsDigit)) return false;

            List<int> JMBG_N = JMBG.Select(x => Int32.Parse(x.ToString())).ToList();

            if (JMBG_N.Count != 13)
                return false;

            else
            {
                Double eval = 0;
                for (int i = 0; i < 6; i++)
                {
                    eval += (7 - i) * (JMBG_N[i] + JMBG_N[i + 6]);
                }
                return JMBG_N[12] == 11 - eval % 11;
            }
        }

        public static Boolean ValidacijaTel(String tel)
        {
            if (tel.ToString().Length != 14) return false;
            else if (!tel[0].ToString().Equals("+")) return false;
            else if (!tel[1].ToString().Equals("3") || !tel[2].ToString().Equals("8") || !tel[3].ToString().Equals("7")) return false;
            else if (!tel[6].ToString().Equals("-") || !tel[10].ToString().Equals("-")) return false;
            return true;
        }
        
        public static Boolean validacijaIme(String i)
        {
            if (!i.All(char.IsLetter)) return false;
            return true;
        }

        public static Boolean validacijaPrezime(String p)
        {
            if (!p.All(char.IsLetter)) return false;
            return true;
        }

        #endregion

    }
}
