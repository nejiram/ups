﻿namespace Zadatak3
{
    partial class USP_Međunarodna_isporuka___BiH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(USP_Međunarodna_isporuka___BiH));
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dokleComboBox = new System.Windows.Forms.ComboBox();
            this.odakleComboBox = new System.Windows.Forms.ComboBox();
            this.cijenaIsporukeButton = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.cijenaTextBox = new System.Windows.Forms.TextBox();
            this.hitnostTrackBar = new System.Windows.Forms.TrackBar();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.takseTextBox = new System.Windows.Forms.TextBox();
            this.tezinaTextBox = new System.Windows.Forms.TextBox();
            this.distancaTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.registrujPosiljkuButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.komentarRichTextBox = new System.Windows.Forms.RichTextBox();
            this.telefonTextBox = new System.Windows.Forms.TextBox();
            this.jmbgTextBox = new System.Windows.Forms.TextBox();
            this.prezimeTextBox = new System.Windows.Forms.TextBox();
            this.imeTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.izbrisiButton = new System.Windows.Forms.Button();
            this.tabelaDataGrid = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hitnostTrackBar)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(889, 182);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(290, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "What can Brown do for you ?";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(890, 210);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(301, 142);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dokleComboBox);
            this.groupBox1.Controls.Add(this.odakleComboBox);
            this.groupBox1.Controls.Add(this.cijenaIsporukeButton);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.cijenaTextBox);
            this.groupBox1.Controls.Add(this.hitnostTrackBar);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.takseTextBox);
            this.groupBox1.Controls.Add(this.tezinaTextBox);
            this.groupBox1.Controls.Add(this.distancaTextBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(8, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 352);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Podaci o pošiljci";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // dokleComboBox
            // 
            this.dokleComboBox.FormattingEnabled = true;
            this.dokleComboBox.Items.AddRange(new object[] {
            "BiH",
            "HR",
            "JPN",
            "SRB",
            "CG",
            "NL",
            "IT"});
            this.dokleComboBox.Location = new System.Drawing.Point(277, 48);
            this.dokleComboBox.Name = "dokleComboBox";
            this.dokleComboBox.Size = new System.Drawing.Size(139, 24);
            this.dokleComboBox.TabIndex = 18;
            // 
            // odakleComboBox
            // 
            this.odakleComboBox.FormattingEnabled = true;
            this.odakleComboBox.Items.AddRange(new object[] {
            "BiH",
            "HR",
            "JPN",
            "SRB",
            "CG",
            "NL",
            "IT"});
            this.odakleComboBox.Location = new System.Drawing.Point(54, 48);
            this.odakleComboBox.Name = "odakleComboBox";
            this.odakleComboBox.Size = new System.Drawing.Size(139, 24);
            this.odakleComboBox.TabIndex = 17;
            // 
            // cijenaIsporukeButton
            // 
            this.cijenaIsporukeButton.Location = new System.Drawing.Point(61, 280);
            this.cijenaIsporukeButton.Name = "cijenaIsporukeButton";
            this.cijenaIsporukeButton.Size = new System.Drawing.Size(322, 50);
            this.cijenaIsporukeButton.TabIndex = 16;
            this.cijenaIsporukeButton.Text = "Izračunaj Cijenu Isporuke";
            this.cijenaIsporukeButton.UseVisualStyleBackColor = true;
            this.cijenaIsporukeButton.Click += new System.EventHandler(this.cijenaIsporukeButton_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(388, 193);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 17);
            this.label17.TabIndex = 15;
            this.label17.Text = "KM";
            // 
            // cijenaTextBox
            // 
            this.cijenaTextBox.Location = new System.Drawing.Point(200, 188);
            this.cijenaTextBox.Name = "cijenaTextBox";
            this.cijenaTextBox.Size = new System.Drawing.Size(182, 22);
            this.cijenaTextBox.TabIndex = 14;
            // 
            // hitnostTrackBar
            // 
            this.hitnostTrackBar.Location = new System.Drawing.Point(15, 203);
            this.hitnostTrackBar.Name = "hitnostTrackBar";
            this.hitnostTrackBar.Size = new System.Drawing.Size(143, 56);
            this.hitnostTrackBar.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(197, 167);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(109, 17);
            this.label16.TabIndex = 12;
            this.label16.Text = "Cijena Isporuke:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 167);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(108, 17);
            this.label15.TabIndex = 11;
            this.label15.Text = "Hitnost Pošiljke:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(388, 139);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 17);
            this.label14.TabIndex = 10;
            this.label14.Text = "%";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(388, 111);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 17);
            this.label13.TabIndex = 9;
            this.label13.Text = "kg";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(388, 81);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 17);
            this.label12.TabIndex = 8;
            this.label12.Text = "km";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(67, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 17);
            this.label11.TabIndex = 7;
            this.label11.Text = "Takse:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 111);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 17);
            this.label10.TabIndex = 6;
            this.label10.Text = "Težina pošiljke:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(51, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 17);
            this.label9.TabIndex = 5;
            this.label9.Text = "Distanca:";
            // 
            // takseTextBox
            // 
            this.takseTextBox.Location = new System.Drawing.Point(139, 134);
            this.takseTextBox.Name = "takseTextBox";
            this.takseTextBox.Size = new System.Drawing.Size(244, 22);
            this.takseTextBox.TabIndex = 4;
            // 
            // tezinaTextBox
            // 
            this.tezinaTextBox.Location = new System.Drawing.Point(141, 106);
            this.tezinaTextBox.Name = "tezinaTextBox";
            this.tezinaTextBox.Size = new System.Drawing.Size(242, 22);
            this.tezinaTextBox.TabIndex = 3;
            // 
            // distancaTextBox
            // 
            this.distancaTextBox.Location = new System.Drawing.Point(139, 78);
            this.distancaTextBox.Name = "distancaTextBox";
            this.distancaTextBox.Size = new System.Drawing.Size(243, 22);
            this.distancaTextBox.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(241, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Do:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Od:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.registrujPosiljkuButton);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.komentarRichTextBox);
            this.groupBox2.Controls.Add(this.telefonTextBox);
            this.groupBox2.Controls.Add(this.jmbgTextBox);
            this.groupBox2.Controls.Add(this.prezimeTextBox);
            this.groupBox2.Controls.Add(this.imeTextBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(444, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(439, 352);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lični podaci";
            // 
            // registrujPosiljkuButton
            // 
            this.registrujPosiljkuButton.Location = new System.Drawing.Point(62, 280);
            this.registrujPosiljkuButton.Name = "registrujPosiljkuButton";
            this.registrujPosiljkuButton.Size = new System.Drawing.Size(322, 51);
            this.registrujPosiljkuButton.TabIndex = 10;
            this.registrujPosiljkuButton.Text = "Registruj Pošiljku";
            this.registrujPosiljkuButton.UseVisualStyleBackColor = true;
            this.registrujPosiljkuButton.Click += new System.EventHandler(this.registrujPosiljkuButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Komentar:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Telefon:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "JMBG:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Prezime:";
            // 
            // komentarRichTextBox
            // 
            this.komentarRichTextBox.Location = new System.Drawing.Point(103, 164);
            this.komentarRichTextBox.Name = "komentarRichTextBox";
            this.komentarRichTextBox.Size = new System.Drawing.Size(296, 96);
            this.komentarRichTextBox.TabIndex = 5;
            this.komentarRichTextBox.Text = "";
            // 
            // telefonTextBox
            // 
            this.telefonTextBox.Location = new System.Drawing.Point(104, 134);
            this.telefonTextBox.Name = "telefonTextBox";
            this.telefonTextBox.Size = new System.Drawing.Size(295, 22);
            this.telefonTextBox.TabIndex = 4;
            // 
            // jmbgTextBox
            // 
            this.jmbgTextBox.Location = new System.Drawing.Point(103, 106);
            this.jmbgTextBox.Name = "jmbgTextBox";
            this.jmbgTextBox.Size = new System.Drawing.Size(296, 22);
            this.jmbgTextBox.TabIndex = 3;
            // 
            // prezimeTextBox
            // 
            this.prezimeTextBox.Location = new System.Drawing.Point(103, 78);
            this.prezimeTextBox.Name = "prezimeTextBox";
            this.prezimeTextBox.Size = new System.Drawing.Size(296, 22);
            this.prezimeTextBox.TabIndex = 2;
            // 
            // imeTextBox
            // 
            this.imeTextBox.Location = new System.Drawing.Point(103, 50);
            this.imeTextBox.Name = "imeTextBox";
            this.imeTextBox.Size = new System.Drawing.Size(296, 22);
            this.imeTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ime:";
            // 
            // izbrisiButton
            // 
            this.izbrisiButton.Location = new System.Drawing.Point(15, 526);
            this.izbrisiButton.Name = "izbrisiButton";
            this.izbrisiButton.Size = new System.Drawing.Size(1176, 62);
            this.izbrisiButton.TabIndex = 5;
            this.izbrisiButton.Text = "Izbriši Unesenu Pošiljku";
            this.izbrisiButton.UseVisualStyleBackColor = true;
            this.izbrisiButton.Click += new System.EventHandler(this.izbrisiButton_Click);
            // 
            // tabelaDataGrid
            // 
            this.tabelaDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelaDataGrid.Location = new System.Drawing.Point(8, 358);
            this.tabelaDataGrid.Name = "tabelaDataGrid";
            this.tabelaDataGrid.RowTemplate.Height = 24;
            this.tabelaDataGrid.Size = new System.Drawing.Size(1183, 162);
            this.tabelaDataGrid.TabIndex = 6;
            this.tabelaDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(992, 42);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(103, 114);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // USP_Međunarodna_isporuka___BiH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1213, 600);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tabelaDataGrid);
            this.Controls.Add(this.izbrisiButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label1);
            this.Name = "USP_Međunarodna_isporuka___BiH";
            this.Text = "USP_Međunarodna_isporuka - BiH";
            this.Load += new System.EventHandler(this.USP_Međunarodna_isporuka___BiH_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hitnostTrackBar)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabelaDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox dokleComboBox;
        private System.Windows.Forms.ComboBox odakleComboBox;
        private System.Windows.Forms.Button cijenaIsporukeButton;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox cijenaTextBox;
        private System.Windows.Forms.TrackBar hitnostTrackBar;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox takseTextBox;
        private System.Windows.Forms.TextBox tezinaTextBox;
        private System.Windows.Forms.TextBox distancaTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button registrujPosiljkuButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox komentarRichTextBox;
        private System.Windows.Forms.TextBox telefonTextBox;
        private System.Windows.Forms.TextBox jmbgTextBox;
        private System.Windows.Forms.TextBox prezimeTextBox;
        private System.Windows.Forms.TextBox imeTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button izbrisiButton;
        private System.Windows.Forms.DataGridView tabelaDataGrid;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}